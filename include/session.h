#ifndef Session_H_
#define Session_H_

#include "user.h"

using namespace std;

namespace ImagesLibrary
{

class Session {
	
private:
	int trial;
	User *user; //utilisateur
	User *userTrying; // utilisateurEssaye
public:
	Session();
	~Session();

    bool Identifier(string customerCode);
	int getTrial();
	User *getUser(); //getUtilisateur
	void End(); //Fermer
};

};

#endif


