#ifndef UTILISATEUR_H_
#define UTILISATEUR_H_

#include <string>
#include <exception>

using namespace std;

namespace ImagesLibrary
{

class User {

private:
	string customerCode;
public:
	User(string customerCode);
	int Level();
	string getCustomerCode();
};

class UnknownUser: public exception
{
	public:
	virtual const char* what() const throw()
	{
		return "Unknown customer code ... ";
	}
};

}

#endif
