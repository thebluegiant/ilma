#ifndef IMAGESLIBRARY_H_
	#define IMAGESLIBRARY_H_

	#include <list>
	#include <string>
	#include <exception>
	#include "image.h"

	namespace ImagesLibrary // Package
	{
		enum Criteria	 // Enumerator for Search & Sort criterias
		{
			TITLE,
			SOURCE,
			NUMBER,
			COST,
			PRIVACYLEVEL,
			LAND
		};

		enum Order		// Enumerator for sorting orders
		{
			ASCENDING,
			DESCENDING
		};

		class Sorting	// Class for sorting images
		{
			private: 
				Criteria criteria;
				Order order;
			public:
				//CONSTRUCTORS
				Sorting();
				Sorting(Criteria criteria, Order order);

				Criteria getCriteria(void); // Signatures of getCriteria & setCriteria functions (Sorting depending on the selected criteria)

				void setCriteria(Criteria criteria);
				

				Order getOrder(void);		// Signatures of getOrder & setOrder functions (Sorting order)
				void setOrder(Order order);

		};

		class Library: public std::list<Image> // Class Library - Inhereting from List
		{
			private:
				int lastNumber;
                std::string fileName;

				//Sorting functions

                static bool title_ascending(Image& first,Image& second);
                static bool title_descending(Image& first,Image& second);
                static bool source_ascending(Image& first,Image& second);
                static bool source_descending(Image& first,Image& second);
                static bool number_ascending(Image& first,Image& second);
                static bool number_descending(Image& first,Image& second);
                static bool cost_ascending(Image& first,Image& second);
                static bool cost_descending(Image& first,Image& second);
                static bool land_ascending(Image& first,Image& second);
                static bool land_descending(Image& first,Image& second);

			public:
				//CONSTRUCTORS
				Library();
				Library(Library &library); 		// Constructor copy


                void Save(void);
				void Save(std::string file);	// file -> file on which the library will be saved (generally a .txt file)

				void Load(std::string file);	// file -> file from which the library will be loaded (also .txt file)


				void Sort(Criteria criteria = NUMBER , Order order = ASCENDING); // Sorting based on chosen criteria and order
				void Sort(Sorting sort);										 // Based on sort criteria

				Library Filter(Criteria criteria,std::string value); //
				Library Filter(Criteria criteria,std::string min,std::string max); //min ->

				Image &at(unsigned int index);

				int getLastNumber(void);
                std::string getFileName(void);
                void setFileName(std::string fileName); // File containing the list of image's names


				void push_back(const Image& val);

				Image &getImage(int numero);
		};

		class LibraryOverflow: public exception
		{
			virtual const char* what() const throw()
			{
				return "Index exceeding library size ... ";
			}
        };
    }

#endif	/* IMAGESLIBRARY_H_ */
