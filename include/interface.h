#ifndef INTERFACE_H_
	#define INTERFACE_H_

	#include <imageslibrary.h>
#include "session.h"
	
	
	namespace ImagesLibrary
	{
		class Interface
		{
			private:
				Library wholeLibrary;
				Library filteredLibrary;
				Sorting sort;
				Session session;

				void newLibrary(void);
				void newImage(void);
				void modifyImage(void);
				void deleteImage(void);
				void showImage(void);
				void showLibrary(void);
				void sortLibrary(void);
				void saveLibrary(void);
				void loadLibrary(void);
				void filterLibrary(void);
				void showHistogram(void);
				void filterImage(void);
			public: 
				Interface();

				bool Menu(void);
				bool Identification(void);
		};
	};

#endif /* INTERFACE_H_ */
