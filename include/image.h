#ifndef IMAGE_H_
	#define IMAGE_H_

	#include <opencv2/core/core.hpp>
	#include "informations.h"

	using namespace std;
	using namespace cv;

	namespace ImagesLibrary
	{
		class Image
		{
			private:
				Informations descriptor;
				Mat content;
			public: 
				Image();
				Image(Informations descriptor);
				Mat getContent(void);
				void setContent(Mat content);
				Informations getDescriptor(void);
				void setDescriptor(Informations descriptor);
				Mat Histogram(void);
				void Filter(Mat custom_kernel);
		};
	};

#endif /* EXAMPLE_H_ */
