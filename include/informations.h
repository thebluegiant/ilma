#ifndef INFORMATIONS_H_
	#define INFORMATIONS_H_

	#include <string>

	using namespace std;

	namespace ImagesLibrary
	{
		class Informations
		{
			private:
				string title;
				string source;
				int number;
				double  cost;
				char privacylevel;
				string filename;
				string land;
			public:
				Informations();
				Informations(string title, string source, int number, double cost, char privacylevel, string filename, string land);
				
				string getTitle(void);
				
				void setTitle(string title);
				
				string getSource(void);
				
				void setSource(string source);
				
				int getNumber(void);
				
				void setNumber(int number);
				
				double getCost(void);
				
				void setCost(double cost);
				
				char getPrivacyLevel(void);
				
				void setPrivacyLevel(char privacylevel);

				string getFileName(void);

				void setFileName(string filename);

				string getLand(void);

				void setLand(string land);
		};
	};
#endif
