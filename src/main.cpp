#include <iostream>

#include "interface.h"

using namespace std;
using namespace ImagesLibrary;

int main(int argc,char *argv[])
{
	Interface interface;

	while(interface.Menu())
		while(!interface.Identification());

	return 0;
}
