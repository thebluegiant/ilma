#include <informations.h>
#include "opencv2/core/core.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"

#include "image.h"

using namespace cv;
using namespace std;
using namespace ImagesLibrary;

Image::Image()
{
}

Image::Image(Informations descriptor):descriptor(descriptor)
{
	string filename = this->descriptor.getFileName();
	Mat img = imread(filename);
	this->content = img;
}

Mat Image::getContent(void)
{ 
	return content;
}

void Image::setContent(Mat content)
{
	this->content=content;
}

Informations Image::getDescriptor(void)
{ 
	return(descriptor);
}

void Image::setDescriptor(Informations descriptor)
{
	this->descriptor=descriptor;
}

Mat Image::Histogram(void)
{
	Mat hist;
	Mat contenu_gray;
	cvtColor(content,contenu_gray,CV_RGB2GRAY);
	int histSize = 256;
	float range[] = { 0, 255 } ;
	const float* histRange = { range };
    calcHist( &content, 1, 0, Mat(),hist, 1, &histSize, &histRange, true, false);
    return hist;
}

void Image::Filter(Mat custom_kernel)
{
	Mat img;
	Point anchor = Point( -1, -1 );		// Central pixel
	double delta = 0;
	int ddepth = -1;
	filter2D(content, img, ddepth , custom_kernel, anchor, delta, BORDER_DEFAULT );
	content = img;
}
