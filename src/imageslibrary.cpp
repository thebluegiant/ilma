#include <imageslibrary.h>
#include <iostream>
#include <fstream>
#include <string>
#include <cstdlib>
#include <exception>
#include <cstring>

#include "opencv2/core/core.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"


using namespace std;
using namespace ImagesLibrary;

//Constructor
Library::Library():list(),lastNumber(0),fileName("")
{
}

//Constructor copy
Library::Library(Library &library):list(library),lastNumber(library.lastNumber),fileName(library.fileName)
{
}


void Library::Save(void)
{
    string file = this->fileName;

    ofstream libraryFile(file.c_str());

    unsigned int dotFile = file.find_last_of(".");

    libraryFile << lastNumber << endl;

    Library::iterator it = this->begin();

    while(it != this->end())
    {
        string fileName = (*it).getDescriptor().getFileName();

        unsigned int firstCharacter = fileName.find_last_of("/\\") + 1;
        unsigned int lastCharacter = fileName.find_last_of(".");

        string imageFileName = file.substr(0,dotFile) + "/" + fileName.substr(firstCharacter,lastCharacter - firstCharacter) + ".txt";
        string image2FileName = file.substr(0,dotFile) + "/" + fileName.substr(firstCharacter,lastCharacter - lastCharacter) + ".jpg";

        try
        {
	        libraryFile << imageFileName;

	        ofstream imageFile(imageFileName.c_str());

	        imageFile << (*it).getDescriptor().getTitle() << endl;
	        imageFile << (*it).getDescriptor().getSource() << endl;
	        imageFile << (*it).getDescriptor().getNumber() << endl;
	        imageFile << (*it).getDescriptor().getCost() << endl;
	        imageFile << (*it).getDescriptor().getPrivacyLevel() << endl;
	        imageFile << image2FileName  << endl;
	        imageFile << (*it).getDescriptor().getLand();

	        cv::imwrite(image2FileName,(*it).getContent());

	        imageFile.close();

	        it++;

	        if(it != this->end())
	        {
	           libraryFile  << endl;
	        }
    	}
    	catch(exception e)
    	{
    		cerr << "Error : Image not saved." << endl;
    	}
    }

    libraryFile.close();
}

void Library::Save(string file)
{
	ofstream libraryFile(file.c_str());

    unsigned int fileDot = file.find_last_of(".");

    libraryFile << lastNumber << endl;

	Library::iterator it = this->begin();

	int i = 0;

	while(it != this->end() && i < this->size())
	{
		string fileName = (*it).getDescriptor().getFileName();

		unsigned int firstCharacter = fileName.find_last_of("/\\") + 1;
		unsigned int lastCharacter = fileName.find_last_of(".");

        string imageFileName = file.substr(0,fileDot - 1) + "/" + fileName.substr(firstCharacter,lastCharacter) + "txt";
        string imageFileName2 = file.substr(0,fileDot - 1) + "/" + fileName.substr(firstCharacter,lastCharacter) + "jpg";

        try
        {
			libraryFile << imageFileName;

			ofstream imageFile(imageFileName.c_str());

			imageFile << (*it).getDescriptor().getTitle() << endl;
			imageFile << (*it).getDescriptor().getSource() << endl;
			imageFile << (*it).getDescriptor().getNumber() << endl;
			imageFile << (*it).getDescriptor().getCost() << endl;
			imageFile << (*it).getDescriptor().getPrivacyLevel() << endl;
			imageFile << imageFileName2 << endl;
			imageFile << (*it).getDescriptor().getLand();

	        cv::imwrite(imageFileName2,(*it).getContent());

			imageFile.close();

			it++;
			i++;

			if(it != this->end())
			{
				libraryFile << endl;
			}
		}
		catch(exception e)
    	{
    		cerr << "Error : Image not saved." << endl;
    	}
	}

	libraryFile.close();

    this->fileName = file;
}

/*
	La fonction Charger

	fichier		-	Le nom du fichier d'où sera chargé la library
*/
void Library::Load(string file)
{
	this->clear();

	ifstream libraryFile(file.c_str());

    //unsigned int pointFichier = fichier.find_last_of(".");

	char lastNumber[80];

	libraryFile.getline(lastNumber,80);

	this->lastNumber = atoi(lastNumber);

	while(!libraryFile.eof())
	{
		char infosFileName[80];

		libraryFile.getline(infosFileName,80);

		string imageFileName(infosFileName);

		ifstream imageFile(imageFileName.c_str());

		char title[80];
		char source[80];
		char number[80];
		char cost[80];
		char privacyLevel[80];
		char fileName[80];
		char land[80];

		imageFile.getline(title,80);
		imageFile.getline(source,80);
		imageFile.getline(number,80);
		imageFile.getline(cost,80);
		imageFile.getline(privacyLevel,80);
		imageFile.getline(fileName,80);
		imageFile.getline(land,80);

		int l = strlen(title);

		if(title[l - 1] == '\r')
			title[l - 1] = '\0';

		l = strlen(source);

		if(source[l - 1] == '\r')
			source[l - 1] = '\0';

		l = strlen(fileName);

		if(fileName[l - 1] == '\r')
			fileName[l - 1] = '\0';

		l = strlen(land);

		if(land[l - 1] == '\r')
			land[l - 1] = '\0';

		Informations descriptor(string(title),string(source),atoi(number),atof(cost),privacyLevel[0],string(fileName),string(land));

		Image img(descriptor);

		this->list::push_back(img);

		imageFile.close();
	}

	libraryFile.close();

    this->fileName = file;
}

void Library::Sort(Criteria criteria,Order order)
{
	switch(criteria)
	{
		case TITLE:
			switch(order)
			{
				case ASCENDING:
					this->sort(title_ascending);
				break;
				case DESCENDING:
					this->sort(title_descending);
				break;
			}
		break;
		case SOURCE:
			switch(order)
			{
				case ASCENDING:
					this->sort(source_ascending);
				break;
				case DESCENDING:
					this->sort(source_descending);
				break;
			}
		break;
		case NUMBER:
			switch(order)
			{
				case ASCENDING:
					this->sort(number_ascending);
				break;
				case DESCENDING:
					this->sort(number_descending);
				break;
			}
		break;
		case COST:
			switch(order)
			{
				case ASCENDING:
					this->sort(cost_ascending);
				break;
				case DESCENDING:
					this->sort(cost_descending);
				break;
			}
		break;
		case LAND:
			switch(order)
			{
				case ASCENDING:
					this->sort(land_ascending);
				break;
				case DESCENDING:
					this->sort(land_descending);
				break;
			}
		break;
		default:
			this->sort(number_ascending);
	}
}

void Library::Sort(Sorting sorting)
{
	switch(sorting.getCriteria())
	{
		case TITLE:
			switch(sorting.getOrder())
			{
				case ASCENDING:
					this->sort(title_ascending);
				break;
				case DESCENDING:
					this->sort(title_descending);
				break;
			}
		break;
		case SOURCE:
			switch(sorting.getOrder())
			{
				case ASCENDING:
					this->sort(source_ascending);
				break;
				case DESCENDING:
					this->sort(source_descending);
				break;
			}
		break;
		case NUMBER:
			switch(sorting.getOrder())
			{
				case ASCENDING:
					this->sort(number_ascending);
				break;
				case DESCENDING:
					this->sort(number_descending);
				break;
			}
		break;
		case COST:
			switch(sorting.getOrder())
			{
				case ASCENDING:
					this->sort(cost_ascending);
				break;
				case DESCENDING:
					this->sort(cost_descending);
				break;
			}
		break;
		case LAND:
			switch(sorting.getOrder())
			{
				case ASCENDING:
					this->sort(land_ascending);
				break;
				case DESCENDING:
					this->sort(land_descending);
				break;
			}
		break;
		default:
			this->sort(number_ascending);
	}
}


Library Library::Filter(Criteria criteria,std::string value)
{
	Library filteredLibrary;

	Library::iterator it = this->begin();

	while(it != this->end())
	{
		switch(criteria)
		{
			case TITLE:
				if((*it).getDescriptor().getTitle().find(value) != string::npos)
				{
					filteredLibrary.list::push_back(*it);
				}
			break;
			case SOURCE:
				if((*it).getDescriptor().getSource().find(value) != string::npos)
				{
					filteredLibrary.list::push_back(*it);
				}
			break;
			case NUMBER:
                if((*it).getDescriptor().getNumber() == atoi(value.c_str()))
				{
                	filteredLibrary.list::push_back(*it);
				}
			break;
			case COST:
                if((*it).getDescriptor().getCost() == atof(value.c_str()))
				{
                	filteredLibrary.list::push_back(*it);
				}
			break;
			case PRIVACYLEVEL:
				if(value[0] == 'R')
				{
					filteredLibrary.list::push_back(*it);
				}
				else if((*it).getDescriptor().getPrivacyLevel() == value[0])
				{
					filteredLibrary.list::push_back(*it);
				}
			break;
			case LAND:
				if((*it).getDescriptor().getLand().find(value) != string::npos)
				{
					filteredLibrary.list::push_back(*it);
				}
			break;
			default:
				filteredLibrary = *this;
		}

		it++;
	}

	return filteredLibrary;
}

Library Library::Filter(Criteria criteria,std::string min,std::string max)
{
	Library filteredLibrary;

	Library::iterator it = this->begin();

	while(it != this->end())
	{
		switch(criteria)
		{
			case NUMBER:
                if((*it).getDescriptor().getNumber() >= atoi(min.c_str()) && (*it).getDescriptor().getNumber() <= atoi(max.c_str()))
				{
                	filteredLibrary.list::push_back(*it);
				}
			break;
			case COST:
                if((*it).getDescriptor().getCost() >= atof(min.c_str()) && (*it).getDescriptor().getCost() <= atof(max.c_str()))
				{
                	filteredLibrary.list::push_back(*it);
				}
			break;
			default:
				filteredLibrary = *this;
		}

		it++;
	}

	return filteredLibrary;
}


Image &Library::at(unsigned int index)
{
    LibraryOverflow libraryOverflow;
	Library::iterator it = this->begin();

	int n = this->size();

	try
	{

		for(register int i = 0;i < n && it != this->end();it++,i++)
			if(i == index)
				return (*it);

		throw libraryOverflow;
	}
	catch(exception &e)
	{
		cerr << e.what() << endl;
	}
}

int Library::getLastNumber(void)
{
	return this->lastNumber;
}


string Library::getFileName(void)
{
    return this->fileName;
}


void Library::setFileName(string fileName)
{
    this->fileName = fileName;
}


void Library::push_back(const Image& val)
{
	Image img = val;

	img.getDescriptor().setNumber(lastNumber + 1);

	lastNumber++;

	list::push_back(img);
}


Image &Library::getImage(int number)
{
	LibraryOverflow libraryOverflow;
	Library::iterator it = this->begin();

	int n = this->size();

	try
	{

		for(register int i = 0;i < n && it != this->end();it++,i++)
			if((*it).getDescriptor().getNumber() == number)
				return (*it);

		throw libraryOverflow;
	}
	catch(exception &e)
	{
		cerr << e.what() << endl;
	}
}


bool Library::title_ascending(Image& first,Image& second)
{
	if(first.getDescriptor().getTitle() < second.getDescriptor().getTitle())
		return true;

	return false;
}

bool Library::title_descending(Image& first,Image& second)
{
	if(first.getDescriptor().getTitle() > second.getDescriptor().getTitle())
		return true;
	
	return false;
}

bool Library::source_ascending(Image& first,Image& second)
{
	if(first.getDescriptor().getSource() < second.getDescriptor().getSource())
		return true;

	return false;
}

bool Library::source_descending(Image& first,Image& second)
{
	if(first.getDescriptor().getSource() > second.getDescriptor().getSource())
		return true;
	
	return false;
}

bool Library::number_ascending(Image& first,Image& second)
{
	if(first.getDescriptor().getNumber() < second.getDescriptor().getNumber())
		return true;

	return false;
}

bool Library::number_descending(Image& first,Image& second)
{
	if(first.getDescriptor().getNumber() > second.getDescriptor().getNumber())
		return true;
	
	return false;
}

bool Library::cost_ascending(Image& first,Image& second)
{
	if(first.getDescriptor().getCost() < second.getDescriptor().getCost())
		return true;

	return false;
}

bool Library::cost_descending(Image& first,Image& second)
{
	if(first.getDescriptor().getCost() > second.getDescriptor().getCost())
		return true;
	
	return false;
}

bool Library::land_ascending(Image& first,Image& second)
{
	if(first.getDescriptor().getLand() < second.getDescriptor().getLand())
		return true;

	return false;
}

bool Library::land_descending(Image& first,Image& second)
{
	if(first.getDescriptor().getLand() > second.getDescriptor().getLand())
		return true;
	
	return false;
}
