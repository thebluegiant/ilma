#include "interface.h"

#include <iostream>
#include <cstdlib>
#include <cfloat>

#include "opencv2/core/core.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"

using namespace cv;
using namespace std;
using namespace ImagesLibrary;

Interface::Interface()
{
	this->wholeLibrary.Load("imageslibrary.txt");

	this->filteredLibrary = this->wholeLibrary;
}

bool Interface::Menu(void)
{
	if(this->session.getUser())
	{
		int level = this->session.getUser()->Level();

		cout << "What would you like to do ? " << endl;

		switch(level)
		{
			case 1:
				cout << "1. Create a new library," << endl;
				cout << "2. Add a new image to the library," << endl;
				cout << "3. Modify an image," << endl;
				cout << "4. Delete an image," << endl;
				cout << "5. Save library," << endl;
				cout << "6. Load library," << endl;
				cout << "7. Show the histogram of an image," << endl;
				cout << "8. Apply a filter to an image," << endl;
			case 2:
				cout << "9. Show an image inside the library," << endl;
				cout << "10. Show library," << endl;
				cout << "11. Sort library," << endl;
				cout << "12. Filter library," << endl;
			default:
				cout << "13. Disconnect," << endl;
				cout << "0. Exit." << endl;
		}

		int myChoice;

		cout << "$ ";
		cin >> myChoice;

		switch(myChoice)
		{
			case 0:
				return false;
			break;
			case 1:
				if(level == 1) this->newLibrary();
			break;
			case 2:
				if(level == 1) this->newImage();
			break;
			case 3:
				if(level == 1) this->modifyImage();
			break;
			case 4:
				if(level == 1) this->deleteImage();
			break;
			case 5:
				if(level == 1) this->saveLibrary();
			break;
			case 6:
				if(level == 1) this->loadLibrary();
			break;
			case 7:
				if(level == 1) this->showHistogram();
			break;
			case 8:
				if(level == 1) this->filterImage();
			break;
			case 9:
				if(level <= 2) this->showImage();
			break;
			case 10:
				if(level <= 2) this->showLibrary();
			break;
			case 11:
				if(level <= 2) this->sortLibrary();
			break;
			case 12:
				if(level <= 2) this->filterLibrary();
			break;
			case 13:
				this->session.End();
			break;
		}
	}

	return true;
}

void Interface::newLibrary(void)
{
	string libraryName;

	cout << "Enter the new library name ?" << endl;
	cin >> libraryName;

	this->wholeLibrary.Save();

	Library neww;

	neww.setFileName(libraryName + ".txt");

	string folder = "mkdir " + libraryName;

	system(folder.c_str());

	this->wholeLibrary = neww;

	this->filteredLibrary = this->wholeLibrary;

	cout << "The new library has been created." << endl;
}

void Interface::newImage(void)
{
	string title;
	string source;
	int number = this->wholeLibrary.getLastNumber() + 1;
	double  imageCost;
	char PrivacyLevel;
	string filename;
	string land;

	cout << "Image title : ";
	cin >> title;
	cout << "Image source : ";
	cin >> source;
	cout << "Image cost: ";
	cin >> imageCost;
	cout << "Image privacy level (Type F if free or R if restricted) : ";
	cin >> PrivacyLevel;
	if(PrivacyLevel != 'F' || PrivacyLevel != 'R')
		PrivacyLevel = 'F';
	cout << "Image file : ";
	cin >> filename;
	cout << "Land or country where the image has been taken : ";
	cin >> land;

	Mat img = imread(filename);

	unsigned int firstCharacter = filename.find_last_of("/\\") + 1;

	unsigned int lastCharacter = this->wholeLibrary.getFileName().find_last_of(".");

	string imageFilename = this->wholeLibrary.getFileName().substr(0,lastCharacter) + "/" + filename.substr(firstCharacter,filename.length());

	imwrite(imageFilename,img);

	Informations informations (title,source,number,imageCost,PrivacyLevel,imageFilename,land);
	Image image(informations);

	this->wholeLibrary.push_back(image);

	this->filteredLibrary = this->wholeLibrary;

	cout << "New image added to library." << endl;
}

void Interface::modifyImage(void)
{
	int number;

	cout << "What image would you like to modify : ";
	cin >> number;

	try
	{
		Image &image = this->wholeLibrary.getImage(number);

		string title;
		string source;
		double  imageCost;
		char privacyLevel;
		string land;

		Informations descriptor = image.getDescriptor();

		cout << "Current image title is " << descriptor.getTitle();
		cin >> title;
		descriptor.setTitle(title);
		cout << "Current image source is : " << descriptor.getSource();
		cin >> source;
		descriptor.setSource(source);
		cout << "Current image cost is: " << descriptor.getCost();
		cin >> imageCost;
		descriptor.setCost(imageCost);
		cout << "Current privacy level (F for free , R for restricted) is : " << descriptor.getPrivacyLevel();
		cin >> privacyLevel;
		if(privacyLevel != 'F' || privacyLevel != 'R')
			privacyLevel = 'F';
		descriptor.setPrivacyLevel(privacyLevel);
		cout << "Current image location (land) is: " << descriptor.getLand();
		cin >> land;
		descriptor.setLand(land);

		image.setDescriptor(descriptor);

		this->filteredLibrary = this->wholeLibrary;

		this->filteredLibrary.Sort(this->sort);

		cout << "Image infos edited" << endl;
	}
	catch(exception e)
	{
		cout << "No matching image." << endl;
	}
}

void Interface::deleteImage(void)
{
	int number;

	cout << "What image would you like to delete ? ";
	cin >> number;

	LibraryOverflow libraryOverflow;
	Library::iterator it = this->wholeLibrary.begin();

	int n = this->wholeLibrary.size();

	try
	{
		for(register int i = 0;i < n && it != this->wholeLibrary.end();it++,i++)
			if((*it).getDescriptor().getNumber() == number)
			{
				this->wholeLibrary.erase(it);

				this->filteredLibrary= this->wholeLibrary;

				this->filteredLibrary.Sort(this->sort);

				cout << "Image deleted." << endl;

				return;
			}

		throw libraryOverflow;
	}
	catch(exception &e)
	{
		cerr << e.what() << endl;
		cout << "Image not found." << endl;
	}
}

void Interface::showImage(void)
{
	int number;

	cout << "What image would you like to see ? ";
	cin >> number;

	try
	{
		Image &image = this->wholeLibrary.getImage(number);

		cout << "Title\t:\t" << image.getDescriptor().getTitle() << endl;
		cout << "Source\t:\t" << image.getDescriptor().getSource() << endl;
		cout << "Number\t:\t" << image.getDescriptor().getNumber() << endl;
		cout << "Cost\t:\t" << image.getDescriptor().getCost() << endl;
		cout << "Privacy Level\t:\t" << image.getDescriptor().getPrivacyLevel() << endl;
		cout << "File Name\t:\t" << image.getDescriptor().getFileName() << endl;
		cout << "Land\t:\t" << image.getDescriptor().getLand() << endl;

		startWindowThread();

		namedWindow(image.getDescriptor().getTitle(), WINDOW_AUTOSIZE);
    	imshow(image.getDescriptor().getTitle(), image.getContent());
    	waitKey(0);

    	destroyWindow(image.getDescriptor().getTitle());

    	return;
	}
	catch(exception e)
	{
		cout << "Image not found." << endl;
	}
}

void Interface::showLibrary(void)
{
	if(this->session.getUser())
	{
		int level = this->session.getUser()->Level();

		if(level == 1)
			this->filteredLibrary = this->filteredLibrary.Filter(PRIVACYLEVEL,"R");
		else if(level == 2)
			this->filteredLibrary = this->filteredLibrary.Filter(PRIVACYLEVEL,"F");
		this->filteredLibrary.Sort(this->sort);

		cout << "    Title " << ((this->sort.getCriteria()==TITLE)?((this->sort.getOrder()==ASCENDING)?'v':'^'):' ')
			 << "  |    Source " << ((this->sort.getCriteria()==SOURCE)?((this->sort.getOrder()==ASCENDING)?'v':'^'):' ')
			 << "  |    Number " << ((this->sort.getCriteria()==NUMBER)?((this->sort.getOrder()==ASCENDING)?'v':'^'):' ')
			 << "  |     Cost " << ((this->sort.getCriteria()==COST)?((this->sort.getOrder()==ASCENDING)?'v':'^'):' ')
			 << "   |     Land " << ((this->sort.getCriteria()==LAND)?((this->sort.getOrder()==ASCENDING)?'v':'^'):' ') << "   " << endl;
		cout << string(73,'-') << endl;

		Library::iterator it = this->filteredLibrary.begin();

		while(it != this->filteredLibrary.end())
		{
			cout << " ";
			string title = (*it).getDescriptor().getTitle().substr(0,11);
			cout << title << ((title.length() < 11)?string(11 - title.length(),' '):"");
			cout << " | ";
			string source = (*it).getDescriptor().getSource().substr(0,12);
			cout << source << ((source.length() < 12)?string(12 - source.length(),' '):"");
			cout << " | ";
			stringstream ssNumber;
			ssNumber << (*it).getDescriptor().getNumber();
   			string number = ssNumber.str();
   			int l = number.length();
   			cout << number << string(12 - l,' ');
   			cout << " | ";
   			stringstream ssCout;
   			ssCout << (*it).getDescriptor().getCost();
   			string imageCost = ssCout.str();
   			l = imageCost.length();
   			cout << imageCost << string(12 - l,' ');
   			cout << " | ";
   			string land = (*it).getDescriptor().getLand().substr(0,12);
   			cout << land << ((land.length() < 12)?string(12 - land.length(),' '):"");
   			cout << " " << endl;

			it++;
		}
	}
}

void Interface::sortLibrary(void)
{
	int criteria;

	cout << "Sort criteria: " << endl;
	cout << "1. Title," << endl;
	cout << "2. Source," << endl;
	cout << "3. Number," << endl;
	cout << "4. Cost," << endl;
	cout << "5. Land." << endl;
	cout << "$ ";
	cin >> criteria;

	string strCriteria;

	switch(criteria)
	{
		case 1:
			this->sort.setCriteria(TITLE);
			strCriteria = "Title";
		break;
		case 2:
			this->sort.setCriteria(SOURCE);
			strCriteria = "Source";
		break;
		case 3:
			this->sort.setCriteria(NUMBER);
			strCriteria = "Number";
		break;
		case 4:
			this->sort.setCriteria(COST);
			strCriteria = "Cost";
		break;
		case 5:
			this->sort.setCriteria(LAND);
			strCriteria = "Land";
		break;
		default:
			this->sort.setCriteria(NUMBER);
			strCriteria = "Number";
	}

	int order;

	cout << "Sorting order : " << endl;
	cout << "1. Ascending," << endl;
	cout << "2. Descending." << endl;
	cout << "$ ";
	cin >> order;

	string strOrder;

	switch(order)
	{
		case 1:
			this->sort.setOrder(ASCENDING);
			strOrder = "Ascending";
		break;
		case 2:
			this->sort.setOrder(DESCENDING);
			strOrder = "Descending";
		break;
		default:
			this->sort.setOrder(ASCENDING);
			strOrder = "Ascending";
	}

	cout << "Library sorted by " << strOrder << " " << strCriteria << "." << endl;

	this->showLibrary();
}

void Interface::saveLibrary(void)
{
	unsigned int lastCharacter = this->wholeLibrary.getFileName().find_last_of(".");

	string libraryName = this->wholeLibrary.getFileName().substr(0,lastCharacter);
	string newName;

	cout << "Current library name is : " << libraryName << endl;
	cin >> newName;

	if(!newName.length())
	{
		this->wholeLibrary.Save();
	}
	else
	{
		this->wholeLibrary.setFileName(newName + ".txt");

		string folder = "mkdir " + newName;

		system(folder.c_str());

		this->filteredLibrary = this->wholeLibrary;

		this->wholeLibrary.Save();
	}

	cout << "Library saved." << endl;
}

void Interface::loadLibrary(void)
{
	string libraryName;

	cout << "What is the library name ?" << endl;
	cin >> libraryName;

	this->wholeLibrary.Save();

	Library neww;

	neww.Load(libraryName+ ".txt");

	this->wholeLibrary = neww;

	this->filteredLibrary = this->wholeLibrary;

	cout << "Library loaded." << endl;
}

void Interface::filterLibrary(void)
{
	int option;

	cout << "Choose a filtering criteria: " << endl;
	cout << "1. Free images," << endl;
	cout << "2. Images costing less than 50 USD," << endl;
	cout << "3. Images costing between 50 and 100 USD," << endl;
	cout << "4. Images costing more than 100 USD," << endl;
	cout << "5. Other." << endl;
	cout << "$ ";
	cin >> option;

	this->filteredLibrary = this->wholeLibrary;

	stringstream ssCost;
	ssCost << DBL_MAX;
	string imageCost = ssCost.str();

	switch(option)
	{
		case 1:
			this->filteredLibrary = this->wholeLibrary.Filter(COST,"0.0");
			this->filteredLibrary.Sort(this->sort);

			cout << "Library filtered." << endl;

			this->showLibrary();
			return;
		break;
		case 2:
			this->filteredLibrary = this->wholeLibrary.Filter(COST,"0.0","50.0");
			this->filteredLibrary.Sort(this->sort);

			cout << "Library filtered." << endl;

			this->showLibrary();
			return;
		break;
		case 3:
			this->filteredLibrary = this->wholeLibrary.Filter(COST,"50.0","100.0");
			this->filteredLibrary.Sort(this->sort);

			cout << "Library filtered." << endl;

			this->showLibrary();
			return;
		break;
		case 4:
			this->filteredLibrary = this->wholeLibrary.Filter(COST,"100.0",imageCost);
			this->filteredLibrary.Sort(this->sort);

			cout << "The library has been filtered." << endl;

			this->showLibrary();
			return;
		break;
	}

	int type;

	cout << "Type of filtering (selection) : " << endl;
	cout << "1. Exact matching ," << endl;
	cout << "2. Interval matching." << endl;
	cout << "$ ";
	cin >> type;

	cout << "Choose a filtering criteria : " << endl;

	int criteria;

	switch(type)
	{
		case 1:
			cout << "1. Title," << endl;
			cout << "2. Source," << endl;
			cout << "3. Land," << endl;
		case 2:
			cout << "4. Number," << endl;
			cout << "5. Cost." << endl;
	}

	cout << "$ ";
	cin >> criteria;

	if(type == 1)	// Exact matching
	{
		string value;

		cout << "Search value : ";
		cin >> value;

		switch(criteria)
		{
			case 1:
				this->filteredLibrary = this->wholeLibrary.Filter(TITLE,value);
			break;
			case 2:
				this->filteredLibrary = this->wholeLibrary.Filter(SOURCE,value);
			break;
			case 3:
				this->filteredLibrary = this->wholeLibrary.Filter(LAND,value);
			break;
			case 4:
				this->filteredLibrary = this->wholeLibrary.Filter(NUMBER,value);
			break;
			case 5:
				this->filteredLibrary = this->wholeLibrary.Filter(COST,value);
			break;
		}
	}
	else if(type == 2)	// Interval matching
	{
		string min, max;

		cout << "Lower bound : ";
		cin >> min;
		cout << "Upper bound : ";
		cin >> max;

		switch(criteria)
		{
			case 4:
				this->filteredLibrary = this->wholeLibrary.Filter(NUMBER,min,max);
			break;
			case 5:
				this->filteredLibrary = this->wholeLibrary.Filter(COST,min,max);
			break;
		}
	}

	this->filteredLibrary.Sort(this->sort);

	cout << "The library has been filtered." << endl;

	this->showLibrary();
}

bool Interface::Identification(void)
{
	if(this->session.getUser())
		return true;

	if(this->session.getTrial() == 3)
		return false;

	string customerCode;

	if(this->session.getTrial() > 0)
		cout << "Trial # " << this->session.getTrial() + 1 << endl;
	cout << "Customer code : ";
	cin >> customerCode;

	return this->session.Identifier(customerCode);
}

void Interface::showHistogram(void)
{
	int number;

	cout << "Image to show (Number): ";
	cin >> number;

	try
	{
		Image &image = this->wholeLibrary.getImage(number);

		Mat hist = image.Histogram();
		int histSize = 256;

		cout << "Title\t:\t" << image.getDescriptor().getTitle() << endl;
		cout << "Source\t:\t" << image.getDescriptor().getSource() << endl;
		cout << "Number\t:\t" << image.getDescriptor().getNumber() << endl;
		cout << "Cost\t:\t" << image.getDescriptor().getCost() << endl;
		cout << "Privacy Level\t:\t" << image.getDescriptor().getPrivacyLevel() << endl;
		cout << "File Name\t:\t" << image.getDescriptor().getFileName() << endl;
		cout << "Land\t:\t" << image.getDescriptor().getLand() << endl;

		startWindowThread();

		namedWindow(image.getDescriptor().getTitle(), WINDOW_AUTOSIZE);
    	imshow(image.getDescriptor().getTitle(), image.getContent());

		int hist_w = 512; int hist_h = 400;
		int bin_w = cvRound( (double) hist_w/histSize );
		Mat histImage( hist_h, hist_w, CV_8UC3, Scalar( 0,0,0) );

		normalize(hist, hist, 0, histImage.rows, NORM_MINMAX, -1, Mat());

		for( int i = 1; i < histSize; i++ )
		{
		  line( histImage, Point( bin_w*(i-1), hist_h - cvRound(hist.at<float>(i-1)) ) ,
		                   Point( bin_w*(i), hist_h - cvRound(hist.at<float>(i)) ),
		                   Scalar( 255, 0, 0), 2, 8, 0  );
		}

		// Displaying results
		namedWindow("Histogram", CV_WINDOW_AUTOSIZE );
		imshow("Histogram", histImage );

    	waitKey(0);

    	destroyWindow(image.getDescriptor().getTitle());
    	destroyWindow("Histogram");

    	return;
	}
	catch(exception e)
	{
		cout << "Image not found." << endl;
	}
}

void Interface::filterImage(void)
{
	int number;

	cout << "Image to filter (Number) : ";
	cin >> number;

	int filter;

	cout << "Filtering with : " << endl;
	cout << "1. Mean," << endl;
	cout << "2. Gaussian," << endl;
	cout << "3. Laplacian." << endl;
	cout << "$ ";
	cin >> filter;

	Mat kernelFilter;
	int kernel_size;

	switch(filter)
	{
		case 1:
			kernel_size = 3;
			kernelFilter = Mat::ones( kernel_size, kernel_size, CV_32F )/ (float)(kernel_size*kernel_size);
		break;
		case 2:
			kernelFilter = getGaussianKernel(3,1.0,CV_32FC1);
		break;
		case 3:
			kernelFilter = (Mat_<float>(3,3) << 0,1,0,1,-4,1,0,1,0);
		break;
	}


	try
	{
		Image &image = this->wholeLibrary.getImage(number);

		cout << "Title\t:\t" << image.getDescriptor().getTitle() << endl;
		cout << "Source\t:\t" << image.getDescriptor().getSource() << endl;
		cout << "Number\t:\t" << image.getDescriptor().getNumber() << endl;
		cout << "Cost\t:\t" << image.getDescriptor().getCost() << endl;
		cout << "Privacy Level\t:\t" << image.getDescriptor().getPrivacyLevel() << endl;
		cout << "File Name\t:\t" << image.getDescriptor().getFileName() << endl;
		cout << "Land\t:\t" << image.getDescriptor().getLand() << endl;

		startWindowThread();

		namedWindow(image.getDescriptor().getTitle(), WINDOW_AUTOSIZE);
    	imshow(image.getDescriptor().getTitle(), image.getContent());

    	image.Filter(kernelFilter);

    	namedWindow("Filtered Image", WINDOW_AUTOSIZE);
    	imshow("Filtered Image", image.getContent());

    	waitKey(0);

    	destroyWindow(image.getDescriptor().getTitle());
    	destroyWindow("Filtered Image");

    	return;
	}
	catch(exception e)
	{
		cout << "Image not found." << endl;
	}
}
