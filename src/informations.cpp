#include <informations.h>

using namespace std;
using namespace ImagesLibrary;

Informations::Informations():title(""), source(""), number(0), cost(0.0), privacylevel('F'), filename(""), land("")
{

}

Informations::Informations(string title, string source, int number, double cost, char privacyLevel, string filename, string land):
				title(title), source(source), number(number), cost(cost), privacylevel(privacyLevel), filename(filename),land(land)
{

}

string Informations::getTitle(void)
{
	return title;
}

void Informations::setTitle(string title)
{
	this->title = title;
}

string Informations::getSource(void)
{
	return source;
}

void Informations::setSource(string source)
{
	this->source = source;
}

int Informations::getNumber(void)
{
	return number;
}

void Informations::setNumber(int number)
{
	this->number = number;
}

double Informations::getCost(void)
{
	return cost;
}

void Informations::setCost(double cost)
{
	this->cost = cost;
}

char Informations::getPrivacyLevel(void)
{
	return privacylevel;
}

void Informations::setPrivacyLevel(char privacyLevel)
{
	if(privacyLevel == 'F' || privacyLevel == 'R')
		this->privacylevel = privacyLevel;
	else
		this->privacylevel = 'F';
}

string Informations::getFileName(void)
{
	return filename;
}

void Informations::setFileName(string filename)
{
	this->filename= filename;
}

string Informations::getLand(void)
{
	return land;
}
void Informations::setLand(string land)
{
	this->land = land;
}
