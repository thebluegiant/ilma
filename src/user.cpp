#include <user.h>
#include <string>
#include <iostream>

using namespace std;
using namespace ImagesLibrary;

User::User(string customerCode):customerCode(customerCode)
{
	UnknownUser unknownUser;

	try
	{
		// Checking code type validity
		if (customerCode.length() == 5 && 'a' <= customerCode[0] && customerCode[0] <= 'z' &&
				'a' <= customerCode[1] && customerCode[1]<= 'z' && '1' <= customerCode[2] &&
				customerCode[2] <= '3' && '0' <= customerCode[3] && customerCode[3] <= '9' &&
				 '1' <= customerCode[4] && customerCode[4] <= '9')
		{
		}
		else
		{
			throw unknownUser;
		}
	}
	catch(UnknownUser &e)
	{
		this->customerCode = "bi300";
		cerr << e.what() << endl;
	}
}

int User::Level()	// bi[1]01	3 possibilities
{
	int N = customerCode[2] - '0'; // Converting to integer
	return N;
}
string User::getCustomerCode()
{
	return customerCode;
}
