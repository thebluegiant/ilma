#include "session.h"

#include <cstdlib>
#include <fstream>

using namespace std;
using namespace ImagesLibrary;

Session::Session():trial(0),user(NULL),userTrying(NULL)
{
}

Session::~Session()
{
    if(user)
    {
        delete user;
        user = NULL;
    }

    if(userTrying)
    {
        delete userTrying;
        userTrying = NULL;
    }
}

bool Session::Identifier(string customerCode)
{
	if(trial >= 3)
		return false;

	if(userTrying) // utilisateurEssaye != NULL
	{
		if(userTrying->getCustomerCode() == customerCode)
		{
			trial++;

			return false;
		}
	}

	ifstream file("user.txt");

	while(!file.eof())
	{
		char str[80];

		file.getline(str,80);

		if(customerCode == string(str))
		{
			if(userTrying)
			{
				delete userTrying;

				userTrying = NULL;
			}

			trial = 0;

			user= new User(customerCode);

			return true;
		}
	}

	file.close();

	if(userTrying)
	{
		delete userTrying;

		userTrying = NULL;
	}

	userTrying= new User(customerCode);

	trial++;

	return false;
}

int Session::getTrial()
{
	return trial;
}

User *Session::getUser()
{
	return user;
}

void Session::End()
{
	if(user)
	{
		delete user;
		user = NULL;
	}

	trial = 0;

	if(userTrying)
	{
		delete userTrying;
		userTrying = NULL;
	}
}
