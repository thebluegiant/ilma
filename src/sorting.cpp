#include <imageslibrary.h>

using namespace std;
using namespace ImagesLibrary;

Sorting::Sorting():criteria(NUMBER),order(ASCENDING)
{
}

Sorting::Sorting(Criteria criteria, Order order):criteria(criteria), order(order)
{
}

Criteria Sorting::getCriteria ()
{
	return (criteria);
}

void Sorting::setCriteria(Criteria criteria)
{
    this->criteria=criteria;
}

Order Sorting::getOrder()
{
	return(order);
}

void Sorting::setOrder(Order order)
{
    this->order=order;
}
